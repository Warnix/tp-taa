package main.java.com.github.jmaillard.mockito.domain;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.cglib.core.Local;
import org.mockito.runners.MockitoJUnitRunner;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class OrderTest {

	
	@Test
	public void testCreationAndPriceOrder() throws Exception {
		//GIVEN
		Order order = Mockito.spy(new Order(null));
		
		Product prod1 = Mockito.mock(Product.class);
		Product prod2 = Mockito.mock(Product.class);
		
		order.products.add(prod1);
		order.products.add(prod2);
		
		//WHEN
		Mockito.when(prod1.getPrice()).thenReturn(new BigDecimal("3.99"));
		Mockito.when(prod2.getPrice()).thenReturn(new BigDecimal("5.00"));
		
		//THEN
		assertEquals(new BigDecimal("8.99"), order.getTotalPrice());
		
	}
	
	@Test
	public void testFormatTotalPrice() throws Exception {
		//GIVEN
		Order order = Mockito.spy(new Order(null));
	
		Product prod1 = Mockito.mock(Product.class);
		Product prod2 = Mockito.mock(Product.class);
		
		Mockito.when(prod1.getPrice()).thenReturn(new BigDecimal("3.99"));
		Mockito.when(prod2.getPrice()).thenReturn(new BigDecimal("5.00"));
		
		order.products.add(prod1);
		order.products.add(prod2);
		
		//WHEN
		String res = order.formatTotalPrice(Locale.FRANCE);
		
		//THEN
		assertEquals("8,99" + (char)160 +"�", order.formatTotalPrice(Locale.FRANCE));		
	}

}
