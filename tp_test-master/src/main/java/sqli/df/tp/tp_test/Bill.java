package sqli.df.tp.tp_test;

public class Bill {
    private int amount;
    private boolean isPaid = false;
    private Person payer;

    public Bill(int amount) {
        this.amount = amount;
    }

    public void paidTheBill(Person payer){
        this.payer = payer;
        this.isPaid = true;
    }

    public int getAmount() {
        return amount;
    }

    public boolean isPayed() {
        return isPaid;
    }

    public Person getPayer() {
        return payer;
    }
}
