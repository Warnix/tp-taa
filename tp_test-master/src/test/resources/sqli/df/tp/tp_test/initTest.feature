#Author: dfrances@sqli.com
Feature: Init test

  Background:
    Given there is a bar named "Le Juste"
    And there is only 10 seats
    And Mr.Pignon and Mr.LeBlanc are going to a bar

  Scenario: scenario 1
    Given there is already 9 persons
    When they arrive at the bar
    Then they are being denied
    And the bar is full

  Scenario: scenario 2.1
    Given there is already 8 persons
    When they arrive at the bar
    And another person arrived at the bar
    Then they are being accepted
    And the last person is denied
    And the bar is full

  Scenario: scenario 3.1
    Given there is already 3 persons
    When they arrive at the bar
    Then they are being accepted

  Scenario Outline: scenario 2.2, 3.2 et 3.3
    Given They order one drink each at 10€
    And Pignon will pay <DrinkToPay1> drink
    And LeBlanc will pay <DrinkToPay2> drink
    And Pignon have drink <numberOfCocktail> Cocktail
    When They have to pay
    Then The bill is correct
    And Pignon pays <DrinkToPay1> drink
    And LeBlanc pays <DrinkToPay2> drink
    And Pignon should be happy : <isHappy>

    Examples:
      | DrinkToPay1 | DrinkToPay2 | numberOfCocktail | isHappy |
      | 0           | 2           | 1                | happy   |
      | 1           | 0           | 1                | happy   |
      | 0           | 3           | 2                | unhappy |




